#include <iostream>
#include <list>
using namespace std;
// Pedro Aisenson, 395/20
using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();

#if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif

  private:
    int mes_;
    int dia_;
};
Fecha::Fecha(int mes, int dia):mes_(mes),dia_(dia){}
int Fecha::mes(){return this->mes_;}
int Fecha::dia(){return this->dia_;}

ostream& operator<<(ostream& os, Fecha r) {
    os << r.dia() << "/" << r.mes();
    return os;
}
void Fecha::incrementar_dia(){
    if (this->dia_== dias_en_mes(this->mes_)){
        this->dia_=1;
        this->mes_++;
}
    else{
        this->dia_++;
    }
}




#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}
#endif

// Ejercicio 11, 12

class Horario{
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);
private:
    uint hora_;
    uint min_;
};
Horario::Horario(uint hora, uint min):hora_(hora),min_(min){}
uint Horario::hora(){return this->hora_;}
uint Horario::min(){return this->min_;}

ostream& operator<<(ostream& os, Horario r) {
    os  << r.hora() << ":" << r.min() ;
    return os;
}
bool Horario::operator==(Horario h){
    return (this->min()==h.min())  &&	 (this->hora()==h.hora());
}
bool Horario::operator<(Horario h) {
if (this->hora()==h.hora()){
    return this->min()<h.min();
}
else return this->hora()<h.hora();
}



// Ejercicio 13

class Recordatorio{
public:
    Recordatorio(Fecha f,Horario h,string m);
    string mensaje();
    Fecha fecha();
    Horario horario();
    bool operator<(Recordatorio r);
private:
    string mensaje_;
    Fecha fecha_;
    Horario horario_;
};
Recordatorio::Recordatorio(Fecha f,Horario h,string m):mensaje_(m),fecha_(f),horario_(h){}
Fecha Recordatorio::fecha(){return this->fecha_;}
Horario Recordatorio:: horario(){return this->horario_;}
string Recordatorio::mensaje() {return this->mensaje_;}
bool Recordatorio::operator<(Recordatorio r) {
   return this->horario()<r.horario();}


ostream& operator<<(ostream& os, Recordatorio r) {
    os  << r.mensaje() << " @ " << r.fecha()<<" "<<r.horario() ;
    return os;}


// Ejercicio 14

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
private:
    Fecha hoy_;
    list<Recordatorio> recordatorios_;
};

Agenda::Agenda(Fecha fecha_inicial):hoy_(fecha_inicial){}
void Agenda ::  incrementar_dia(){this->hoy_.incrementar_dia();}
void Agenda :: agregar_recordatorio(Recordatorio rec){this->recordatorios_.push_back(rec);}
list<Recordatorio> Agenda :: recordatorios_de_hoy(){
    list<Recordatorio> rechoy;
    for(Recordatorio i : recordatorios_){
        if (i.fecha()==this->hoy_){
            rechoy.push_back(i);
        }
    }


    rechoy.sort();
     return rechoy;
}
Fecha Agenda :: hoy(){return this->hoy_;}

ostream& operator<<(ostream& os, Agenda a) {
    os  << a.hoy() <<endl <<"====="<<endl;
    for (auto v:a.recordatorios_de_hoy()){
        os << v<<endl;
    }
    return os;}
