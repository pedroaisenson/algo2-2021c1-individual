#include "Lista.h"
Lista::Lista() {
    _ultimo = nullptr;
    _primero= nullptr;
    _longitud = 0;

}

Lista::Lista(const Lista& l) : Lista() {
    while (0<_longitud){
        this->eliminar(0);
    }
    _ultimo = nullptr;
    _primero= nullptr;
    _longitud = 0;

    Nodo* PunteroANodoCopiado = l._primero;
    for (int i = 0 ; i <l._longitud;i++){
        this->agregarAtras(PunteroANodoCopiado->contenido);
        PunteroANodoCopiado = PunteroANodoCopiado->prox;
    }
}

Lista::~Lista() {

while (0<_longitud){
    this->eliminar(0);
}
    delete _ultimo ;
    delete _primero;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    while (0<_longitud){
        this->eliminar(0);
    }
    _ultimo = nullptr;
    _primero= nullptr;
    _longitud = 0;

    Nodo* PunteroANodoCopiado = aCopiar._primero;
    for (int i = 0 ; i <aCopiar._longitud;i++){
        this->agregarAtras(PunteroANodoCopiado->contenido);
        PunteroANodoCopiado = PunteroANodoCopiado->prox;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* NuevoNodo= new Nodo;
    NuevoNodo->contenido = elem;
    NuevoNodo->anterior= nullptr;
    NuevoNodo->prox = _primero;
    _primero= NuevoNodo;
    if (_ultimo == nullptr){
        _ultimo=NuevoNodo;
    }
    _longitud ++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo *NuevoNodo= new Nodo;
    NuevoNodo->contenido = elem;
    NuevoNodo->anterior= (this->_ultimo);
    NuevoNodo->prox = nullptr;
    if (_ultimo != nullptr){_ultimo->prox = NuevoNodo;}

    _ultimo = NuevoNodo;
    if (_primero == nullptr){
        _primero=NuevoNodo;

}
    _longitud ++;
}
void Lista::eliminar(Nat i) {

Nodo* PunteroANodo = _primero;
if (_longitud==1){
    delete _primero ;
    _primero = nullptr;
    _ultimo = nullptr;
    _longitud = 0;
    return ;
}
    for (int j=0;j<i;j++){
PunteroANodo = PunteroANodo->prox;
}
    if (PunteroANodo->anterior!=nullptr){
           (PunteroANodo->anterior)->prox = PunteroANodo->prox;
    }
    else{
        _primero = PunteroANodo->prox;
        (PunteroANodo->prox)->anterior=nullptr;
    }
    if ((PunteroANodo->prox)!=nullptr){
        (PunteroANodo->prox)->anterior = PunteroANodo->anterior;
    }
    else{
        _ultimo = PunteroANodo -> anterior;
        (PunteroANodo->anterior)->prox = nullptr;
    }
    delete PunteroANodo;
    _longitud=_longitud-1;
}

int Lista::longitud() const {

    return _longitud;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* PunteroANodo = _primero;
    for (int j=0;j<i;j++){
        PunteroANodo = PunteroANodo->prox;
    }
    return (PunteroANodo->contenido);
}

int& Lista::iesimo(Nat i) {
    Nodo* PunteroANodo = _primero;
    for (int j=0;j<i;j++){
        PunteroANodo = PunteroANodo->prox;
    }
    return (PunteroANodo->contenido);


}

void Lista::mostrar(ostream& o) {
    // Completar
}
