template <typename T>
string_map<T>::string_map():raiz(new Nodo),_size(0){

    // COMPLETAR
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    for (int i =0;i<raiz->siguientes.size();i++) {
        if(raiz->siguientes[i]!= nullptr)
            borrarPrivado(raiz->siguientes[i]);

    }
    for (int i = 0; i <d.claves_.size() ; ++i) {
       this->insert(make_pair(d.claves_[i],d.at(d.claves_[i])));
    }
}






template <typename T>
string_map<T>::~string_map(){
    vector<Nodo*> v_null (256, nullptr);
    if(raiz->siguientes==v_null){
        delete raiz;
        return;
    }
    else

        for (int i =0;i<raiz->siguientes.size();i++) {
            if(raiz->siguientes[i]!= nullptr)
                borrarPrivado(raiz->siguientes[i]);

        }
    delete raiz;

}
template<typename T>
void string_map<T>::borrarPrivado(Nodo* &ptr){
    for (int j =0;j<ptr->siguientes.size();j++) {
        if (ptr->siguientes[j]!= nullptr)
            borrarPrivado(ptr->siguientes[j]);
    }
    if (ptr != nullptr){
        if (ptr->definicion != nullptr){
            delete ptr->definicion;
            ptr->definicion = nullptr;

        }
        delete ptr;
        ptr = nullptr;
    }










    /*vector<Nodo*> v_null (256, nullptr);
    if(raiz->siguientes==v_null){
        delete raiz;
    }
    else
        for (auto i:raiz->siguientes) {
            if(i!= nullptr)
                borrarPrivado(i);
            else
                i++;
        }
*/
}



/*
template<typename T>
void string_map<T>::borrarPrivado(Nodo* &ptr){
    vector<Nodo*> v_null (256, nullptr);
    if (ptr->siguientes==v_null){
        delete ptr->definicion;
        delete ptr;
        ptr= nullptr;
    }
    else{
        for (auto j:ptr->siguientes) {
            if (j!= nullptr)
                borrarPrivado(j);
            else
                j++;
        }
    }
}
*/

template<typename T>
void string_map<T>::insert(const pair<string, T>& par) {
    // caso donde no hay arbol hecho
    string palabra = par.first;
    T* clave= new T;
    *clave=par.second;
    Nodo* nodo_actual = raiz;
    int indice = 0;
    if (raiz == nullptr) {
        raiz = new Nodo;
        nodo_actual = raiz;
    }
    while (indice < palabra.size()) {
        if (nodo_actual->siguientes[int(palabra[indice])] == nullptr) {
            nodo_actual->siguientes[int(palabra[indice])] = new Nodo;
            nodo_actual = nodo_actual->siguientes[int(palabra[indice])];
            indice++;
        }
            // no hace falta agregarlo, entonces continuamos el recorrido
        else {
            nodo_actual = nodo_actual->siguientes[int(palabra[indice])];
            indice++;
        }
    }
    // si salio quiere decir que recorrio toda la palabra, entonces le asignamos la definicion;
    if (nodo_actual->definicion != nullptr){
        delete nodo_actual->definicion;
        nodo_actual->definicion = nullptr;
    }
    nodo_actual->definicion = clave;
    _size++;
    claves_.push_back(par.first);
}






template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}


template <typename T>
int string_map<T>::count(const string& clave) const{    // lo que hago es voy recorriendo la palabra y me fijo en la posicion correspondiente a la letra si apunta a algo, cando no me quedan letras llegue a donde tendria que estar la definicion
    if(raiz== nullptr)
        return 0;
    string copia_clave=clave;
    int res = count_privada(copia_clave);
    return res;
    // COMPLETAR
}

template <typename T>
int string_map<T>::count_privada( string &clave ) const {
    int indice = 0;
    Nodo *actual = raiz;
    while (indice < clave.size()) {
        if (actual->siguientes[int(clave[indice])] != nullptr) {
            actual = actual->siguientes[int(clave[indice])];
            indice++;
        } else
            return 0;
    }
    if (actual->definicion != nullptr)
        return 1;
    else
        return 0;

}




template <typename T>
const T& string_map<T>::at(const string& clave) const {
    int indice=0;
    Nodo* actual=raiz;
    while(indice<clave.size()){
        actual=actual->siguientes[int(clave[indice])];
        indice ++;
    }
    // cuando sale del while nodo actual es donde esta la clave.
    return  *(actual->definicion);

    // COMPLETAR
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    int indice=0;
    Nodo* actual=raiz;
    while(indice<clave.size()){
        actual=actual->siguientes[int(clave[indice])];
        indice ++;
    }
    // cuando sale del while nodo actual es donde esta la clave.
    return  *(actual->definicion);
    // COMPLETAR
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    int indice = 0;
    Nodo *actual = raiz;
    vector<Nodo *> v_null(256, nullptr);
    while (indice < clave.size() - 1) {
        actual = actual->siguientes[int(clave[indice])];  // actual aca va justo hasta la ultima letra, soea el nodo antes a la definicion
        indice++;
    }
    Nodo *borrar = actual->siguientes[int(clave[indice])];   // el nodo que tiene la definicion
    if (borrar->siguientes == v_null) {
        delete borrar->definicion;
        delete borrar;
        actual->siguientes[int(clave[indice])] = nullptr;// el de arriba que apuntaba a ese hay que borrarlo
        if (actual->siguientes == v_null &&
            actual->definicion == nullptr) {// me fijo si tengo que borrar el camino de esa clave
            for (int i = 1; i <= clave.size(); ++i) {
                actual = raiz;
                indice = 0;
                while (indice < clave.size() - i) {
                    actual = actual->siguientes[int(clave[indice])];
                    indice++;
                }
                actual->siguientes[int(clave[indice])] = nullptr;
                if (actual != raiz && actual->siguientes == v_null && actual->definicion == nullptr) {
                    delete actual->definicion;
                    delete actual;
                } else
                    break;          // este caso es si no borro nada entonces listo // si es la raiz con que el primer elemento de la clave apunte a null esta bien.
            }
        }
    }
    else {
        delete borrar->definicion;
        borrar->definicion = nullptr;
    }
    _size--;
    for (int i = 0; i <claves_.size() ; ++i) {
        if(claves_[i]==clave)
            claves_.erase(claves_.begin()+i);
    }
}


template <typename T>
int string_map<T>::size() const{
    return _size;
    // COMPLETAR
}

template <typename T>
bool string_map<T>::empty() const{
   if(_size==0)
       return true;
   else
       return false;


    // COMPLETAR
}