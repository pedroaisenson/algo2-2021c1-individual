//ABB
template <class T>

Conjunto<T>::Conjunto() {
    _raiz= nullptr;
    _cardinal = 0;
}

template <class T>
Conjunto<T>::~Conjunto() {
        if (_raiz == nullptr) return;
        while (this->cardinal()>1){
            if (this->minimo() != _raiz->valor){
            this->remover(this->minimo());
            }
            else {
                this->remover(this->siguiente(this->minimo()));
            }
    }
        delete _raiz;

}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    if (_raiz==nullptr){return false;}
    Nodo b = *_raiz;
    while ((clave > b.valor and b.der != nullptr) or (clave < b.valor and b.izq != nullptr)){

        if (clave > b.valor ){
            b=*(b.der);
            continue;
        }
        if (clave < b.valor ){
            b=*(b.izq);

        }
    }
    if (b.valor==clave){
        return true;
    }
    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (_raiz==nullptr){_raiz = new Nodo(clave);
        _cardinal += 1;
    return;}

    Nodo* b = _raiz;
    while ((clave > b->valor and b->der != nullptr) or clave < b->valor and b->izq != nullptr){

        if (clave > b->valor ){
            b=b->der;
            continue;
        }
        if (clave < b->valor ){
            b=b->izq;
            continue;
        }
    }
    if (b->valor==clave){
        return;
    }
    if (clave > b->valor ){
        b->der = new Nodo(clave);
    }
    if (clave < b->valor ){
        b->izq = new Nodo(clave);
    }
_cardinal += 1;
}



template <class T>
void Conjunto<T>::remover(const T& clave) {
    bool t = clave == _raiz->valor;
    if (_cardinal ==1 and clave == _raiz->valor){
        _cardinal = 0;
        delete _raiz;
        _raiz=nullptr;
        return;
    }

    Nodo* b = _raiz;
    Nodo* a = b;
    while ((clave > b->valor and b->der != nullptr) or clave < b->valor and b->izq != nullptr){
        a = b;
        if (clave > b->valor ){
            b=b->der;
            continue;
        }
        if (clave < b->valor ){
            b=b->izq;
            continue;
        }
    }
    if (b->valor != clave){
        return;
    }
//Tenemos el elemento en b

//Caso elemento 0 o 1 hijo
    if ( b->der == nullptr or b->izq == nullptr){

   // caso 0 hijos
if ( b->der == nullptr and  b->izq == nullptr){
    if (a->der == b ){
        a ->der = nullptr;
}
    else{a ->izq = nullptr;
}
    delete b; _cardinal -= 1; return;
}
//caso 1 hijo
else {
            if (a->der == b ){
                if (b->der != nullptr) {
                    a->der = b ->der;
                }
                if (b->izq != nullptr) {
                    a->der = b ->izq;
                }
            }
            else{
                if (b->der != nullptr) {
                    a->izq = b ->der;
                }
                if (b->izq != nullptr) {
                    a->izq = b ->izq;
                }
            }

            if (t)_raiz=b->izq;
            delete b; _cardinal -= 1; return;
}


    }
    T s = this->siguiente(b->valor);
    this->remover(s);
    b->valor= s;


}
template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* a ;
    Nodo* b = _raiz;
    while ((clave > b->valor and b->der != nullptr) or (clave < b->valor and b->izq != nullptr)){

        if (clave > b->valor ){
            b=b->der;
        }
        if (clave < b->valor ){
            a= b;
            b=b->izq;
        }
    }
    if (b->der != nullptr){
        b = b->der;
        while (b->izq != nullptr){
            b=b->izq;
        }
        return b->valor;
    }
    else {return a->valor;}
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* b = _raiz;
    while (b->izq != nullptr){
        b=b->izq;
    }
    return b->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* b = _raiz;
    while (b->der != nullptr){
        b=b->der;
    }
    return b->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {

    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

